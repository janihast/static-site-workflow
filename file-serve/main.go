package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

func main() {
	fmt.Printf("Starting server\n")
	port := os.Getenv("PORT")
	// site := os.Getenv("SITE")
	// root := os.Getenv("ROOT")
	// db_host := os.Getenv("DB_HOST")
	// db_port := os.Getenv("DB_PORT")
	// db_name := os.Getenv("DB_NAME")
	// db_user := os.Getenv("DB_USER")
	// db_password := os.Getenv("DB_PASSWORD")
	// db_sslmode := os.Getenv("DB_SSLMODE")

	listen := fmt.Sprintf("0.0.0.0:%s", port)

	r := http.NewServeMux()
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		//host := r.Host
		//uri := r.RequestURI
		fmt.Printf("request: %+v\n", r)

		http.ServeFile(w, r, "main.go")
		// // Read file into memory
		// fileBytes, err := ioutil.ReadAll(f)
		// if err != nil {
		//     log.Println(err)
		//     _, _ = fmt.Fprintf(w, "Error file bytes")
		//     return
		// }
		//
		// // Check mime type
		// mime := http.DetectContentType(fileBytes)
		// if mime != "audio/mpeg" {
		//     log.Println("Error file type")
		//     _, _ = fmt.Fprintf(w, "Error file type")
		//     return
		// }
		//
		// // Custom headers
		// r.Header.Add("Content-Type", "audio/mpeg")
		// r.Header.Add("Cache-Control", "must-revalidate, post-check=0, pre-check=0")
		// r.Header.Add("Content-Description", "File Transfer")
		// r.Header.Add("Content-Disposition", "attachment; filename=file.mp3")
		// r.Header.Add("Content-Transfer-Encoding", "binary")
		// r.Header.Add("Expires", "0")
		// r.Header.Add("Pragma", "public")
		// r.Header.Add("Content-Length", strconv.Itoa(len(fileBytes)))
		// http.ServeFile(w, r, pathFile)
	})
	log.Fatal(http.ListenAndServe(listen, r))
}

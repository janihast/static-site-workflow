module authorized-keys-command

go 1.15

require (
	github.com/go-pg/pg v8.0.7+incompatible
	github.com/jinzhu/inflection v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.4.5 // indirect
	mellium.im/sasl v0.2.1 // indirect
)

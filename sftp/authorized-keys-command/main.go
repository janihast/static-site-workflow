package main

import (
	"crypto/tls"
	"fmt"
	"os"

	"github.com/go-pg/pg"
)

func main() {
	options := pg.Options{}

	username := os.Args[1]

	host := os.Getenv("DB_HOST")
	if len(host) <= 0 {
		host = "localhost"
	}

	port := os.Getenv("DB_PORT")
	if len(port) <= 0 {
		port = "5432"
	}

	options.Addr = fmt.Sprintf("%s:%s", host, port)

	name := os.Getenv("DB_NAME")
	options.Database = name

	user := os.Getenv("DB_USER")
	options.User = user

	password := os.Getenv("DB_PASSWORD")
	options.Password = password

	sql := os.Getenv("QUERY_KEYS_BY_NAME")
	if len(sql) <= 0 {
		fmt.Fprintf(os.Stderr, "QUERY_KEYS_BY_NAME environment variable required\n")
		os.Exit(1)
	}

	sslmode := os.Getenv("DB_SSLMODE")
	if len(sslmode) <= 0 {
		sslmode = "verify-full"
	}

	switch sslmode {
	case "verify-ca", "verify-full":
		options.TLSConfig = &tls.Config{}
	case "allow", "prefer", "require":
		options.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	case "disable":
		options.TLSConfig = nil
	default:
		fmt.Fprintf(os.Stderr, "sslmode `%s` is not supported", sslmode)
		os.Exit(1)
	}

	db := pg.Connect(&options)

	var keys []struct {
		Value string
	}

	_, err := db.Query(&keys, sql, username)
	if err != nil {
		fmt.Fprintf(os.Stderr, "unable to query ssh keys for account `%s`: %s\n", username, err.Error())
		os.Exit(1)
	}

	for _, key := range keys {
		fmt.Printf("%s\n", key.Value)
	}
}

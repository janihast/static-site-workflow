module.exports = {
	devServer: {
		// It is pretty hard to know in docker-compose environment that what ever url
		// developer wanted to use. Nginx proxy ports are mapped only to localhost so
		// this check should never be relevant.
		disableHostCheck: true

		// In case you want to use the address, this is how it goes
		// allowedHosts: [
		//         'ui.devel-flow.local'
		// ]
	},

	transpileDependencies: [
		'vuetify'
	]
}

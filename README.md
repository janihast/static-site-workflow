# Starting up the development environment

## Setup your dns

Use your preferred method to make testing dns addresses work. My method was:
```
$ cat /etc/NetworkManager/dnsmasq.d/devel-flow.local
address=/devel-flow.local/127.0.0.1

$ systemctl restart NetworkManager
```

## Start the environment

With command `docker-compose up` you get environment running. After everything is running, run `bin/add-current-account` which creates your unix user to database, puts all authorization keys for the account and creates home folder to docker volume.

## Create user

Now all is set up, you can go in with `sftp -P 2222 <your username>@.devel-flow.local` and see the static site from browser with the address `http://<your username>.devel-flow.local:8080`.

# Tips / FAQ

## Password can be generated like..

```
openssl passwd -6 -salt xyz password
```

## Why not put configurations as environment variables?

We allow user to login into container, so we try our best to not let user see secrets.

## Debug bash scripts

`bash -x myscript.sh`

# Architecture / Data warehouse

Idea is that we have Cepth bucket(k8s persistent volume) which holds all the data and sftp/webserver use both that. Sftp needs to be able to write in there, and webserver is readonly. This style makes it easier to serve sites most resource reserving way.

We have only one installation of webserver, and scale that for all the sites. Having installation eg. per site we would have a lot of overhead and used resources even if those sites weren't in any use.

Sftp is like HA version since istio routes traffic to multiple pods instead on in some specific k8s worker. And since Ceph can be HA version, the data should never dissappear.

# Further ideas

This model is only for developing purposes. For production usage we likely need to invent some production workflow so that ppl doesn't need to code in production. Having git support for "get things in production" workflow would be cool.

# Links

https://github.com/jandd/libnss-pgsql/blob/master/README
https://github.com/jandd/libnss-pgsql/blob/master/conf/dbschema.sql
https://fusionforge.org/plugins/mediawiki/wiki/fusionforge/index.php/Configuration/NSS
https://www.cyberciti.biz/faq/understanding-etcshadow-file/
https://command-not-found.com/envsubst
https://www.cyberciti.biz/tips/setting-off-password-aging-expiration.html
https://blog.scalesec.com/just-in-time-ssh-provisioning-7b20d9736a07
https://github.com/go-pg/pg
https://stribika.github.io/2015/01/04/secure-secure-shell.html
https://manpages.debian.org/testing/libpam-script/pam-script.7.en.html
https://wiki.archlinux.org/index.php/pam_mount

#!/usr/bin/env bash

set -e

user_name=${USER}
group_name=${USER}
home=${USER}
password='$6$xyz$ShNnbwk5fmsyVIlzOf8zEg4YdEH2aWRSuY4rJHbzLZRlWcoXbxxoI0hfn0mdXiJCdBJ/lTpKjk.vu5NZOv0UM0'

# Get or update group
group_id=$(docker run --rm -it --network=devel-flow --env PGHOST=postgres --env PGUSER=user --env PGPASSWORD=password postgres:latest psql -qtAX database -c "SELECT id FROM \"group\" WHERE name = '${group_name}'" | sed -e 's/[\n\r]//g')
if [ -z "${group_id}" ]; then
	group_id=$(uuidgen)

	echo "Creating group"
	docker run --rm -it --network=devel-flow --env PGHOST=postgres --env PGUSER=user --env PGPASSWORD=password postgres:latest psql database -c "INSERT INTO \"group\" (id, name) VALUES ('${group_id}', '${group_name}')"
fi

# Get or update account
user_id=$(docker run --rm -it --network=devel-flow --env PGHOST=postgres --env PGUSER=user --env PGPASSWORD=password postgres:latest psql -qtAX database -c "SELECT id FROM account WHERE name = '${user_name}'" | sed -e 's/[\n\r]//g')
if [ -z "${user_id}" ]; then
	user_id=$(uuidgen)

	echo "Creating account"
	docker run --rm -it --network=devel-flow --env PGHOST=postgres --env PGUSER=user --env PGPASSWORD=password postgres:latest psql database -c "INSERT INTO account (id, name, home, \"group\") VALUES ('${user_id}', '${user_name}', '${home}', '${group_id}')"
	
	echo "Creating password"
	docker run --rm -it --network=devel-flow --env PGHOST=postgres --env PGUSER=user --env PGPASSWORD=password postgres:latest psql database -c "INSERT INTO account_shadow (account, password) VALUES ('${user_id}', '${password}')"
fi

# Update password for account (I know, doing it doubled first time)
echo "Updating password"
docker run --rm -it --network=devel-flow --env PGHOST=postgres --env PGUSER=user --env PGPASSWORD=password postgres:latest psql database -c "UPDATE account_shadow SET password = '${password}' WHERE account = '${user_id}'"

# Append ssh keys(does not delete old ones, this is for testing purposes after all)
echo "Append ssh keys"
for i in ~/.ssh/*.pub; do
	content=$(cat $i)
	exists=$(docker run --rm -it --network=devel-flow --env PGHOST=postgres --env PGUSER=user --env PGPASSWORD=password postgres:latest psql database -c "SELECT 1 FROM account_ssh_key WHERE account = '${user_id}' AND value = '${content}'" | sed -e 's/[\n\r]//g')
	if [ ! -z "${exists}" ]; then
		docker run --rm -it --network=devel-flow --env PGHOST=postgres --env PGUSER=user --env PGPASSWORD=password postgres:latest psql database -c "INSERT INTO account_ssh_key (account, value) VALUES ('${user_id}', '${content}')"
	fi
done

# Get user uid
user_uid=$(docker run --rm -it --network=devel-flow --env PGHOST=postgres --env PGUSER=user --env PGPASSWORD=password postgres:latest psql -qtAX database -c "SELECT uid FROM account WHERE id = '${user_id}'" | sed -e 's/[\n\r]//g')

# Get user gid
user_gid=$(docker run --rm -it --network=devel-flow --env PGHOST=postgres --env PGUSER=user --env PGPASSWORD=password postgres:latest psql -qtAX database -c "SELECT g.gid FROM account a INNER JOIN \"group\" g ON (a.group = g.id) WHERE a.id = '${user_id}'" | sed -e 's/[\n\r]//g')

# Create home folder and se permissions
echo "Adding home folder"
docker run --rm -it --network=devel-flow --volume=${PWD}/example:/example --volume=devel-flow-sites:/home bash:5.1.4 -c "mkdir -p /home/${home}/public && cp /example/* /home/${home}/public/ && chown -R ${user_uid}:${user_gid} /home/${home}/public"

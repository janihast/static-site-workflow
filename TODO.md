# SFTP

* sftp: Make sure ssh-authorized-key-command doesn't expose database secrets for anyone else except root
* sftp: Make sure environment variables does not give away secrets from environment variables(use some configuration file instead)
* sftp: make container tests that checks that configurations/secrets is readable only by root
* sftp: make tests which checks that authentication work with password/authorization-keys and of'course that not anyone can log into
* sftp: consider changing ubuntu to debian/alpine since pam wasn't actually needed(both nss&pam was only found from ubuntu)

# Documentation

* documentation: make some nice medium article out of this concept of sftp stuff

# Serve

* serving site: instead nginx we likely want own software se we can do more complex home folder stuff https://stackoverflow.com/a/58655915

# Graphile

* secure listeners

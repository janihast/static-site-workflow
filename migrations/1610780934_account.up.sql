BEGIN;
	CREATE EXTENSION pgcrypto;

	CREATE SEQUENCE gid MINVALUE 1000 MAXVALUE 2147483647 NO CYCLE;
	CREATE SEQUENCE uid MINVALUE 1000 MAXVALUE 2147483647 NO CYCLE;
	CREATE TABLE "group" (
		id UUID NOT NULL DEFAULT gen_random_uuid(),
		gid INTEGER NOT NULL DEFAULT nextval('gid'),
		name VARCHAR NOT NULL,
		description TEXT,
		CONSTRAINT pk_group PRIMARY KEY (id)
	);
	CREATE UNIQUE INDEX uq_group_name ON "group" (name);
	CREATE UNIQUE INDEX uq_group_gid ON "group" (gid);

	CREATE TABLE account (
		id UUID NOT NULL DEFAULT gen_random_uuid(),
		"uid" INTEGER NOT NULL DEFAULT nextval('uid'),
		name VARCHAR NOT NULL,
		home VARCHAR NOT NULL,
		password VARCHAR,
		"group" UUID NOT NULL,
		CONSTRAINT pk_account PRIMARY KEY (id),
		CONSTRAINT fk_account_group FOREIGN KEY ("group") REFERENCES "group" (id) ON UPDATE CASCADE ON DELETE CASCADE
	);
	CREATE UNIQUE INDEX uq_account_name ON account (name);
	CREATE UNIQUE INDEX uq_account_uid ON account ("uid");

	CREATE TABLE account_ssh_key (
		id UUID NOT NULL DEFAULT gen_random_uuid(),
		account UUID NOT NULL,
		value VARCHAR NOT NULL,
		CONSTRAINT pk_account_ssh_key PRIMARY KEY (id)
	);
	CREATE INDEX ind_account_ssh_key_account ON account_ssh_key (account);

	CREATE TABLE group_account (
		id UUID NOT NULL DEFAULT gen_random_uuid(),
		account UUID NOT NULL,
		"group" UUID NOT NULL,
		CONSTRAINT pk_group_account PRIMARY KEY (id),
		CONSTRAINT fk_group_account_account FOREIGN KEY (account) REFERENCES account(id) ON UPDATE CASCADE ON DELETE CASCADE,
		CONSTRAINT fk_group_account_group FOREIGN KEY ("group") REFERENCES "group"(id) ON UPDATE CASCADE ON DELETE CASCADE
	);
	CREATE INDEX ind_group_account_account ON group_account(account);
	CREATE INDEX ind_group_account_group ON group_account("group");

	CREATE TABLE account_shadow (
		id UUID NOT NULL DEFAULT gen_random_uuid(),
		account UUID NOT NULL,
		password VARCHAR NOT NULL,
		last_change TIMESTAMP NOT NULL DEFAULT now(),
		minimum INTEGER NOT NULL DEFAULT 0, 
		maximum INTEGER NOT NULL DEFAULT 99999,
		warn INTEGER NOT NULL DEFAULT 0,
		inactive INTEGER NOT NULL DEFAULT -1,
		expire TIMESTAMP,
		flag INTEGER NOT NULL DEFAULT 0,
		CONSTRAINT pk_account_shadow PRIMARY KEY (id),
		CONSTRAINT fk_account_shadow_account FOREIGN KEY (account) REFERENCES account(id) ON UPDATE CASCADE ON DELETE CASCADE
	);
	CREATE UNIQUE INDEX uq_account_shadow ON account_shadow(account);
	COMMENT ON COLUMN account_shadow.minimum IS 'The minimum number of days required between password changes i.e. the number of days left before the user is allowed to change his/her password';
	COMMENT ON COLUMN account_shadow.maximum IS 'The maximum number of days the password is valid (after that user is forced to change his/her password)';
	COMMENT ON COLUMN account_shadow.warn IS 'The number of days before password is to expire that user is warned that his/her password must be changed';
	COMMENT ON COLUMN account_shadow.inactive IS 'The number of days after password expires that account is disabled';
	COMMENT ON COLUMN account_shadow.expire IS 'Days since Jan 1, 1970 that account is disabled i.e. an absolute date specifying when the login may no longer be used';
	COMMENT ON COLUMN account_shadow.flag IS 'Not used';
COMMIT;


